/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cat.mpl.camel.component;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;

import cat.mpl.basex.connection.BaseXConnection;
import cat.mpl.basex.connection.BaseXConnectionPool;

public class BaseXProducer extends DefaultProducer {

	public BaseXProducer(Endpoint endpoint) {
		super(endpoint);
	}

	@Override
	public void process(Exchange exchange) throws Exception {

		BaseXEndpoint endpoint = (BaseXEndpoint) getEndpoint();
		String dataBase = endpoint.getDataBase();

		BaseXComponent component = (BaseXComponent) endpoint.getComponent();
		BaseXConnectionPool connectionPool = component.getConnectionPool();
		BaseXConnection connection = connectionPool.getConnection(dataBase);

		String path = endpoint.getPath();

		if (path != null && !path.isEmpty()) {
			InputStream in = exchange.getIn().getBody(InputStream.class);
			connection.send(path, in);
		} else {

			if (exchange.getIn().getHeaders().containsKey("BASEX_XQUERY")) {
				String xquery = exchange.getIn().getHeader("BASEX_XQUERY").toString();
				OutputStream out = new ByteArrayOutputStream();
				connection.query(xquery, out);
				exchange.getIn().setBody(out, OutputStream.class);
			} else {
				throw new Exception("Unknown operation");
			}

		}

		connectionPool.releaseConnection(connection);
	}

}
