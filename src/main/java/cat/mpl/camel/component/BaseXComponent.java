/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cat.mpl.camel.component;

import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;

import cat.mpl.basex.connection.BaseXConnectionPool;

public class BaseXComponent extends DefaultComponent {

	private BaseXConnectionPool connectionPool = null;

	private String host;
	private int port;
	private String username;
	private String password;

	public BaseXComponent() {
		super();
	}

	public BaseXComponent(CamelContext camelContext) {
		super(camelContext);
	}

	@Override
	protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> properties) throws Exception {

		if (connectionPool == null)
			connectionPool = new BaseXConnectionPool(host, port, username, password);

		BaseXEndpoint endpoint = new BaseXEndpoint(uri, remaining, this);
		setProperties(endpoint, properties);
		return endpoint;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BaseXConnectionPool getConnectionPool() {
		return connectionPool;
	}

}
