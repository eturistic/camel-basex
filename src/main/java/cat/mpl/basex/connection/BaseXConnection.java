/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cat.mpl.basex.connection;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cat.mpl.basex.client.BaseXClient;
import cat.mpl.basex.client.BaseXClient.Query;

public class BaseXConnection implements Closeable {

	private final static Logger LOGGER = LoggerFactory.getLogger(BaseXConnection.class);

	private BaseXClient baseXClient;

	private String dataBase;

	public BaseXConnection(final String host, final int port, final String dataBase, final String username,
			final String password) throws IOException {
		this.dataBase = dataBase;
		baseXClient = new BaseXClient(host, port, username, password);
		open();
	}

	private void open() throws IOException {
		baseXClient.execute("check " + dataBase);
		baseXClient.execute("set defaultdb yes");
	}

	@Override
	public void close() throws IOException {
		baseXClient.close();
	}

	public boolean isAlive() {
		return baseXClient.isConnected();
	}

	public void send(String path, InputStream in) throws IOException {
		long s = System.currentTimeMillis();
		baseXClient.replace(path, in);
		LOGGER.debug("BaseX command executed in " + (System.currentTimeMillis() - s) + "ms");
	}

	public void query(String queryString, OutputStream out) throws IOException {
		long s = System.currentTimeMillis();

		Query query = baseXClient.query(queryString);
		while (query.more()) {
			out.write(query.next().getBytes());
			out.write("\n".getBytes());
		}
		query.close();

		LOGGER.debug("BaseX command executed in " + (System.currentTimeMillis() - s) + "ms");
	}

	public String execute(String command) throws IOException {
		return baseXClient.execute(command);
	}

	public String getDataBase() {
		return dataBase;
	}

	public static void main(String[] args) throws IOException {

		BaseXConnection connection = new BaseXConnection("test.eturistic.com", 1984, "PRH11", "admin", "admin");

		InputStream in = new ByteArrayInputStream("<x>Hello	World!</x>".getBytes());
		connection.send("test/test1.xml", in);

		in = new ByteArrayInputStream("<x>Hello	World!</x>".getBytes());
		connection.send("test/test2.xml", in);

		connection.close();
	}

}
