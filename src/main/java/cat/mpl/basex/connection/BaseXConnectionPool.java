/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cat.mpl.basex.connection;

import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

public class BaseXConnectionPool {

	private HashMap<String, Stack<BaseXConnection>> connections = new HashMap<String, Stack<BaseXConnection>>();

	private String host;
	private int port;
	private String username;
	private String password;

	public BaseXConnectionPool(final String host, final int port, final String username, final String password) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
	}

	public synchronized BaseXConnection getConnection(String dataBase) throws IOException {

		if (!connections.containsKey(dataBase))
			connections.put(dataBase, new Stack<BaseXConnection>());
		Stack<BaseXConnection> connectionStack = connections.get(dataBase);

		boolean poolWasEmpty = connectionStack.isEmpty();
		
		if (poolWasEmpty)
			connectionStack.push(createConnection(dataBase));

		BaseXConnection connection = connectionStack.pop();

		if (!connection.isAlive()) {
			connection.close();
			connection = new BaseXConnection(dataBase, port, dataBase, username, password);
		}

		try {
			connection.execute("show sessions");						
		} catch (IOException ex) {						
			if(!poolWasEmpty)
				return getConnection(dataBase);
			else
				throw new IOException("Cannot get BaseX connection");
		}

		return connection;
	}

	private BaseXConnection createConnection(String dataBase) throws IOException {
		return new BaseXConnection(host, port, dataBase, username, password);
	}

	public synchronized void releaseConnection(BaseXConnection connection) {

		String dataBase = connection.getDataBase();

		if (!connections.containsKey(dataBase))
			connections.put(dataBase, new Stack<BaseXConnection>());
		Stack<BaseXConnection> connectionStack = connections.get(dataBase);

		connectionStack.push(connection);
	}
}
