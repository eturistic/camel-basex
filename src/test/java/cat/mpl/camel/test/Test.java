/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cat.mpl.camel.test;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.main.Main;

import cat.mpl.camel.component.BaseXComponent;

public class Test {

	public static void main(String[] args) throws Exception {

		CamelContext context = new DefaultCamelContext();
		BaseXComponent baseXComponent = new BaseXComponent(context);
		context.addComponent("basex", baseXComponent);

		baseXComponent.setHost("test.eturistic.com");
		baseXComponent.setPort(1984);
		baseXComponent.setUsername("admin");
		baseXComponent.setPassword("admin");

		context.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				// from("file:///tmp/camel-test").to("basex:test/foo/file2.xml");

				from("timer://timer?repeatCount=1").setHeader("BASEX_XQUERY")
						.constant("declare namespace ota='http://www.opentravel.org/OTA/2003/05';"
								+ "collection('foo')//ota:OTA_HotelResNotifRQ[.//ota:HotelReservationID/@ResID_Value = '24469832']/db:path(.)")
						.to("basex:test").to("stream:out");
			}
		});

		Main main = new Main();
		main.getCamelContexts().add(context);

		main.run();
	}

}
